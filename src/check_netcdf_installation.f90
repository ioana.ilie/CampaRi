! -----------------------------------------------------------------------------
! This function aims to reduce the fringe regions by collapsing the tree leaves
! on their branches. In this way we have less grouping of fringe regions
!
subroutine check_netcdf_installation(wanting_netcdf_backend)
  use mod_gutenberg
  use mod_variables_gen
  implicit none
  logical, intent(inout) :: wanting_netcdf_backend
#ifndef LINK_NETCDF
  call spr('------------------------------------------------------------------')
  call spr('ATTENTION: netcdf memory handling was not installed! DEACTIVATED.')
  call spr('------------------------------------------------------------------')
  wanting_netcdf_backend = .false.
#endif
end
