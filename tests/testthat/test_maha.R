context('mahalanobis distance')

test_that('Using the pipeline for finding the mahalanobis distance', {
  
  # ------------------- =
  # setting up the test
  silent <- T
  plt_stff <- !silent
  require(testthat); require(CampaRi); require(tictoc)
  # ------------------- =
  
  # test mahalanobis distance
  n_elem <- 1000
  n_feats <- 20
  
  c1 <- matrix(rnorm(n_elem*n_feats), nrow = n_elem, ncol = n_feats)
  c2 <- matrix(rnorm(n_elem*n_feats), nrow = n_elem, ncol = n_feats)
  c3 <- matrix(rnorm(n_elem*n_feats), nrow = n_elem, ncol = n_feats)
  # c1 <- matrix(rbeta(n_elem*n_feats, shape1 = 0.5, shape2 = 0.5), nrow = n_elem, ncol = n_feats)
  # c2 <- matrix(rbeta(n_elem*n_feats, shape1 = 0.5, shape2 = 0.5), nrow = n_elem, ncol = n_feats)
  # c3 <- matrix(rbeta(n_elem*n_feats, shape1 = 0.5, shape2 = 0.5), nrow = n_elem, ncol = n_feats)
  c3[, 1:5] <- c3[, 1:5] + 3
  c2[, 3:5] <- c2[, 3:5] - 2
  c1[, 3:5] <- c1[, 3:5] - 2
  ann <- c(rep(1,n_elem*2), rep(2,n_elem))
  if(plt_stff){
    library(ggfortify)
    ggplot(data.frame(data = rbeta(100, shape1 = 0.5, shape2 = 0.5))) + geom_density(aes(x = data))
    df <- cbind(as.data.frame(rbind(c1,c2,c3)), as.factor(c(rep("set1", n_elem), rep("set2", n_elem), rep("set3", n_elem))))
    colnames(df)[ncol(df)] <- "Cluster"
    aplot <- prcomp(as.matrix(df[,1:(ncol(df)-1)]))
    ag <- autoplot(aplot, data = df, colour = "Cluster")
    ggsave("/home/dgarolini/Documents/Pictures/clusterspcaMaha.png", plot = ag)
    c1 <- aplot$x[1:n_elem, 1:2]
    c2 <- aplot$x[101:n_elem, 1:2]
    c3 <- aplot$x[201:n_elem, 1:2]
  }
  # line_search_alpha
  ev_it(Xad <- CampaRi::find_mahaR(c1[1:100,1:5], c2[1:100,1:5], c3[1:100,1:5], K = 10, J = 5, tol = 10^(-5), C = 1, loss_f = "Hinge", silent = silent, line_search_alpha = T), s = silent)
  ev_it(Xad <- CampaRi::find_mahaR(c1[1:100,1:5], c2[1:100,1:5], c3[1:100,1:5], K = 10, J = 5, tol = 10^(-5), C = 1, loss_f = "Huber", silent = silent, verbose = !silent, line_search_alpha = FALSE), s = silent)
  
  # find the distance
  # ev_it(Xad <- CampaRi::find_mahaR(c1, c2, c3, K = 10, J = 5, tol = 10^(-5), C = 1, loss_f = "Hinge", silent = silent, verbose = !silent, line_search_alpha = FALSE), s = silent)
  ev_it(Xad <- CampaRi::find_mahaR(c1, c2, c3, K = 10, J = 5, tol = 10^(-5), C = 1, loss_f = "Hinge", silent = silent, verbose = !silent, line_search_alpha = FALSE), s = silent)
  ev_it(maha_like <- CampaRi::find_maha_likeRat(c1, c2, c3, silent = silent), s = silent)
  
  # ev_it(Xad <- CampaRi::find_mahalanobis(c1, c2, c3, mute_fortran = silent), s = silent) # not really working!!
  
  # create MST - maha
  ev_it(adjl <- mst_from_trj(trj = rbind(c1,c2,c3), distance_method = 12, dump_to_netcdf = T, metric_mat = Xad, return_tree_in_r = T, mute_fortran = silent, silent = silent), s = silent)
  ret <- gen_progindex(adjl, snap_start = 1, silent = silent)
  ret2 <- gen_annotation(ret, snap_start = 1, silent = silent)
  # create MST - maha - like
  ev_it(adjl <- mst_from_trj(trj = rbind(c1,c2,c3), distance_method = 12, dump_to_netcdf = T, metric_mat = maha_like, return_tree_in_r = T, mute_fortran = silent, silent = silent), s = silent)
  ret <- gen_progindex(adjl, snap_start = 3, silent = silent)
  ret2 <- gen_annotation(ret, snap_start = 3,  silent = silent)
  # create MST - euclidean
  adjl <- mst_from_trj(trj = rbind(c1,c2,c3), distance_method = 5, return_tree_in_r = T, mute_fortran = T, silent = silent)
  ret <- gen_progindex(adjl, snap_start = 2, silent = silent)
  ret2 <- gen_annotation(ret, snap_start = 2, silent = silent)
  # plot SAPPHIRE
  a <- sapphire_plot(sap_file = "REPIX_000000000001.dat", ann_trace = ann, timeline = T, title = "Mahalanobis with dynamic programming", return_plot = T, silent = silent) # should be better
  b <- sapphire_plot(sap_file = "REPIX_000000000003.dat", ann_trace = ann, timeline = T, title = "Mahalanobis with prob. estimation", return_plot = T, silent = silent) # should be BETTER 2
  c <- sapphire_plot(sap_file = "REPIX_000000000002.dat", ann_trace = ann, timeline = T, title = "Euclidean", return_plot = T, silent = silent) # should be worse
  
  # abc <- cowplot::plot_grid(c,b,a, nrow = 1); abc
  # ggsave(plot = abc, "/home/dgarolini/Documents/Pictures/sapphires_maha.png", width = 18, height = 6, dpi = 360)
  
# find barriers and score it
  ba1 <- nSBR("REPIX_000000000001.dat", ny = 20, unif.splits = seq(5, 12, 3), pk_span = 200, optimal_merging = T, consec_merging = T, plot = T, silent = silent)
  sc1 <- score_sapphire("REPIX_000000000001.dat", ann = ann, manual_barriers = ba1$barriers_sel, plot_pred_true_resume = T, silent = silent)
  ba3 <- nSBR("REPIX_000000000003.dat", ny = 20, unif.splits = seq(5, 12, 3), pk_span = 200, optimal_merging = T, consec_merging = T, plot = T, silent = silent)
  sc3 <- score_sapphire("REPIX_000000000003.dat", ann = ann, manual_barriers = ba3$barriers_sel, plot_pred_true_resume = T, silent = silent)
  ba2 <- nSBR("REPIX_000000000002.dat", ny = 20, unif.splits = seq(5, 12, 3), pk_span = 200, optimal_merging = T, consec_merging = T, plot = T, silent = silent)
  sc2 <- score_sapphire("REPIX_000000000002.dat", ann = ann, manual_barriers = ba2$barriers_sel, plot_pred_true_resume = T, silent = silent)
  # expect_true(sc1$score.out > sc2$score.out) # I dunno but it is working and not randomly
  # expect_true(sc3$score.out > sc2$score.out)
  
  CampaRi::loop_file_rm(c("REPIX_000000000001.dat", "REPIX_000000000002.dat", "REPIX_000000000003.dat", "MST_DUMPLING.nc"))
  
  # OPTIMIZATION ===================================================
  if(F){
    profvis::profvis(Xad <- CampaRi::find_mahaR(c1, c2, c3, K = 10, J = 5, tol = 10^(-5), C = 1, loss_f = "Hinge"))
    # The simplest is to use crossprod which is the same as t(a)%*% b (Note - this will only be a small increase in speed)
    library(Rfast)
    a <- matrnorm(10000, 100)
    b <- matrnorm(100, 100)
    c <- matrnorm(10000, 100)
    # 
    # f <- function(){
    #   A_fin <- array(0, dim = c(ncol(a), ncol(a)))
    #   for(x in 1:nrow(a)) A_fin <- A_fin + (a[x,]) %*% t(c[x,])
    # }
    # 
    # tmp <- microbenchmark::microbenchmark(#"FRA" = purrr::reduce(simplify2array(apply(abind::abind(a, c, along = 3), 1, function(x) {list("olachico" = (x[,1] %*% t(x[,2])))})), `+`),
    #                                       "FRA2" = rowSums(simplify2array(simplify2array(apply(abind::abind(a, c, along = 3), 1, function(x) {list("olachico" = (x[,1] %*% t(x[,2])))}))), dims = 2),
    #                                       "FOR" = f(), times = 10)
    # vettore <- simplify2array(apply(abind::abind(a, c, along = 3), 1, function(x) {list("olachico" = (x[,1] %*% t(x[,2])))}))
    # profvis::profvis(purrr::reduce(vettore, `+`))
    # 
    gigino2 <- sum(rowSums(a * (c %*% t(b))))
    gigino <- sum(sapply(1:nrow(a), function(x) return(t(a[x,]) %*% b %*% c[x,])))
    gigino3 <- sum(a * (c %*% t(b)))
    mean(gigino - gigino2); range(gigino - gigino2)
    
    tmp <- microbenchmark::microbenchmark(
      # "crossprod" = sum(sapply(1:nrow(a), function(x) return(crossprod(crossprod(a[x,], b), c[x,])))), 
                                          # "%*%" = sum(sapply(1:nrow(a), function(x) return(t(a[x,]) %*% b %*% c[x,]))),
                                          # "%*%2" = sum(sapply(1:nrow(a), function(x) return(t(a[x,]) %*% (b %*% c[x,])))),
                                          "FRA" = sum(rowSums(a * (c %*% t(b)))),
                                          "FRA2" = sum(a * (c %*% t(b))), times = 50)
    x <- rnorm(100000)
    ifelse(x >= 0, 0, x^2) - pmin(0, x)^2
    tmp <- microbenchmark::microbenchmark("ifelse" = ifelse(x >= 0, 0, x^2),
                                          "pmin" = pmin(0, x)^2)
    
    
    library(ggplot2); library(tictoc)
    autoplot(tmp)
    
    # speed relationship for number of feats and snapshots
    detictoc <- function(tt){
      return(tt$toc - tt$tic)
    }
    run_piper <- function(n, m, kkk = 10){
      cat("Doing ( n , m , k) : (", n, ",", m, ",", kkk, ")\n")
      c1 <- Rfast::matrnorm(n, m)
      c2 <- Rfast::matrnorm(n, m)
      c3 <- Rfast::matrnorm(n, m)
      tic()
      Xad <- CampaRi::find_mahaR(c1, c2, c3, K = kkk, J = 5, tol = 10^(-5), C = 1, silent = T, loss_f = "Hinge")
      return(detictoc(toc()))
    }
    nns <- round(seq(500, 100000, length.out = 20))
    timing_n <- sapply(nns, function(x) run_piper(x, 50))  
    ggplot(data = data.frame(nns, timing_n)) + geom_line(aes(x = nns, y = timing_n)) + theme_classic()
    mms <- round(seq(10,10000, length.out = 20))
    timing_m <- sapply(mms, function(x) run_piper(100000, x))    
    kks <- round(seq(10,120, length.out = 10))
    timing_k <- sapply(kks, function(x) run_piper(10000, 50, kkk =x))  
    ggplot(data = data.frame(kks, timing_k)) + geom_line(aes(x = kks, y = timing_k)) + theme_classic()
    
    
    # TESTS FOR FEATURE SELECTION!
    # load the library for comparisons
    library(mlbench); library(caret)
    data(PimaIndiansDiabetes)
    head(PimaIndiansDiabetes)
    # prepare training scheme
    control <- trainControl(method="repeatedcv", number=10, repeats=3)
    # train the model
    model <- train(diabetes~., data=PimaIndiansDiabetes, method="lvq", preProcess="scale", trControl=control)
    # estimate variable importance
    importance <- varImp(model, scale=FALSE)
    # summarize importance
    print(importance)
    # plot importance
    plot(importance)
    to_do <- PimaIndiansDiabetes
    levels(to_do$diabetes) <- c(1, 0)
    to_do$diabetes <- as.numeric(to_do$diabetes)
    head(to_do)
    to_do <- as.matrix(to_do)
    # to_do <- .normalize(to_do)
    maha_m <- array(0, dim = c(ncol(to_do), ncol(to_do)))
    sa2 <- to_do[to_do[, "diabetes"] == 1, ]
    sa1 <- to_do[to_do[, "diabetes"] == 2, ]
    for(i in 1:100){
      # cc1 <- as.matrix(to_do[sample(1:nrow(to_do), size = round(nrow(to_do)/10)),])
      
      cc1 <- as.matrix(sa1[sample(1:nrow(sa1), size = round(nrow(sa1)/20)),])
      # cc2 <- as.matrix(to_do[sample(1:nrow(to_do), size = round(nrow(to_do)/10)),])
      cc2 <- as.matrix(sa1[sample(1:nrow(sa1), size = round(nrow(sa1)/20)),])
      # cc3 <- as.matrix(to_do[sample(1:nrow(to_do), size = round(nrow(to_do)/10)),])
      cc3 <- as.matrix(sa2[sample(1:nrow(sa2), size = nrow(cc2)),])
      
      maha_m <- maha_m + find_mahaR(cc1, cc2, cc3, K = 10, J = 5, C = 1, loss_f = "Hinge", verbose = F)
    }
    pcaout <- princomp(to_do); pcaout$loadings
    ggplot(data.frame("mm" = diag(maha_m), "what" = as.factor(names(PimaIndiansDiabetes)))) + geom_point(aes(x = what, y = mm)) + theme_classic()
  }
})
